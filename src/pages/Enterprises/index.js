import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  View,
  TouchableOpacity,
  TextInput,
  Text,
  Image,
  ScrollView,
  FlatList,
  Picker,
  ActivityIndicator,
} from 'react-native';

import Icon from 'react-native-vector-icons/SimpleLineIcons';
import * as EnterprisesActions from '../../store/modules/enterprises/actions';
import { colors } from '../../styles';
import styles from './styles';

class Enterprises extends Component {
  state = {
    enterprise: '',
    enterpriseType: '',
    searchActive: false,
    loading: false,
  };

  componentDidMount() {
    this.loadAllEnterprises();
  }

  clearFilters = () => {
    this.setState({ enterprise: '', enterpriseType: '' });
    this.loadAllEnterprises();
  };

  loadAllEnterprises = () => {
    const { loadEnterprisesRequest } = this.props;
    const { client, uid, accesstoken } = this.props.auth;

    loadEnterprisesRequest(client, uid, accesstoken);
  };

  loadFilteredEnterprises = () => {
    const { loadFilteredEnterprisesRequest } = this.props;
    const { client, uid, accesstoken } = this.props.auth;
    const { enterprise, enterpriseType } = this.state;

    loadFilteredEnterprisesRequest(
      client,
      uid,
      accesstoken,
      enterpriseType,
      enterprise
    );
  };

  openDetail = id => {
    const { navigation, loadEnterprisesDetailsRequest } = this.props;
    const { client, uid, accesstoken } = this.props.auth;

    loadEnterprisesDetailsRequest(client, uid, accesstoken, id);

    navigation.navigate('EnterpriseDetail');
  };

  renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => this.openDetail(item.id)}>
      <View style={styles.listItem}>
        {item.photo != null ? (
          <Image
            style={styles.listImage}
            source={{ uri: `http://empresas.ioasys.com.br${item.photo}` }}
            resizeMode="cover"
          />
        ) : (
          <Image
            style={styles.listImage}
            source={require('../../img/imgnull.png')}
            resizeMode="cover"
          />
        )}

        <View style={styles.viewTitle}>
          <Text style={styles.textName}>{item.enterprise_name}</Text>
          <Text style={styles.textType}>
            {item.enterprise_type.enterprise_type_name}
          </Text>
        </View>
        <View style={{ alignContent: 'flex-end' }}>
          <Icon name="arrow-right" size={18} color={colors.regular} />
        </View>
      </View>
    </TouchableOpacity>
  );

  render() {
    const { enterprise, enterpriseType, searchActive, loading } = this.state;
    const { enterprisesList } = this.props;

    return (
      <View>
        {searchActive ? (
          <View>
            <View style={styles.boxHeader}>
              <View style={styles.header}>
                <TouchableOpacity
                  onPress={() => this.setState({ searchActive: false })}
                  style={styles.headerItem}
                >
                  <Image
                    style={styles.headerIcon}
                    source={require('../../img/icon.png')}
                    resizeMode="contain"
                  />
                </TouchableOpacity>

                <View style={styles.headerViewTitle}>
                  <Text style={styles.headerTextTitle}>Buscar empresas</Text>
                </View>

                <TouchableOpacity
                  onPress={() => this.clearFilters()}
                  style={styles.headerItem}
                >
                  <Icon name="action-undo" size={20} color={colors.white} />
                </TouchableOpacity>
              </View>
              <View style={styles.searchForm}>
                <View style={styles.viewPicker}>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    style={styles.picker}
                    placeholder="Escolha o segmento"
                    placeholderStyle={{ color: colors.regular }}
                    placeholderIconColor={colors.white}
                    selectedValue={enterpriseType}
                    onValueChange={value =>
                      this.setState({ enterpriseType: value })
                    }
                  >
                    <Picker.Item value="0" label="Escolha o segmento" />
                    <Picker.Item value="1" label="Agro" />
                    <Picker.Item value="2" label="Aviation" />
                    <Picker.Item value="3" label="Biotech" />
                    <Picker.Item value="4" label="Eco" />
                    <Picker.Item value="5" label="Ecommerce" />
                    <Picker.Item value="6" label="Education" />
                    <Picker.Item value="7" label="Fashion" />
                    <Picker.Item value="8" label="Fintech" />
                    <Picker.Item value="9" label="Food" />
                    <Picker.Item value="10" label="Games" />
                    <Picker.Item value="11" label="Health" />
                    <Picker.Item value="12" label="IOT" />
                    <Picker.Item value="13" label="Logistics" />
                    <Picker.Item value="14" label="Media" />
                    <Picker.Item value="15" label="Mining" />
                    <Picker.Item value="16" label="Products" />
                    <Picker.Item value="17" label="Real Estate" />
                    <Picker.Item value="18" label="Service" />
                    <Picker.Item value="19" label="Smart City" />
                    <Picker.Item value="20" label="Social" />
                    <Picker.Item value="21" label="Software" />
                    <Picker.Item value="22" label="Technology" />
                    <Picker.Item value="23" label="Tourism" />
                    <Picker.Item value="24" label="Transport" />
                  </Picker>
                </View>

                <TextInput
                  style={styles.input}
                  autoCapitalize="none"
                  autoCorrect={false}
                  placeholder="Informe o nome da empresa"
                  underlineColorAndroid={colors.transparent}
                  selectionColor={colors.primary}
                  value={enterprise}
                  onChangeText={text => this.setState({ enterprise: text })}
                  returnKeyType="search"
                  onSubmitEditing={() => this.loadFilteredEnterprises()}
                  blurOnSubmit
                />
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => this.loadFilteredEnterprises()}
                >
                  {loading ? (
                    <ActivityIndicator size="small" color={colors.white} />
                  ) : (
                    <Text style={styles.buttonText}>PESQUISAR</Text>
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.buttonUp}
                  onPress={() => this.setState({ searchActive: false })}
                >
                  <Icon name="arrow-up" size={16} color={colors.white} />
                </TouchableOpacity>
              </View>
            </View>
            <ScrollView>
              <FlatList data={enterprisesList} renderItem={this.renderItem} />
            </ScrollView>
          </View>
        ) : (
          <View>
            <View style={styles.header}>
              <View style={styles.headerItem} />

              <Image
                style={styles.headerViewTitle}
                source={require('../../img/logowhite.png')}
                resizeMode="contain"
              />

              <TouchableOpacity
                onPress={() => this.setState({ searchActive: true })}
                style={styles.headerItem}
              >
                <Icon name="magnifier" size={20} color={colors.white} />
              </TouchableOpacity>
            </View>
            <ScrollView>
              <FlatList data={enterprisesList} renderItem={this.renderItem} />
            </ScrollView>
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  enterprisesList: state.enterprises.enterprisesList.enterprises,
  auth: state.auth,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(EnterprisesActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Enterprises);
