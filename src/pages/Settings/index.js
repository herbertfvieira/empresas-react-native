import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Text,
  Image,
} from 'react-native';

import Icon from 'react-native-vector-icons/SimpleLineIcons';
import * as LoginActions from '../../store/modules/auth/actions';

import { colors } from '../../styles';
import styles from './styles';

const menu = [
  {
    id: 1,
    icon: 'info',
    title: 'Sobre o App',
    route: 'About',
  },
  {
    id: 2,
    icon: 'logout',
    title: 'Sair do App',
    route: 'Login',
  },
];

class Settings extends Component {
  static navigationOptions = {
    header: null,
  };

  routingPage = route => {
    const { navigation, signOut } = this.props;

    if (route === 'Login') {
      signOut();
    } else {
      navigation.navigate(route);
    }
  };

  renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => this.routingPage(item.route)}>
      <View style={styles.listItem}>
        <View style={styles.viewTitle}>
          <Text style={styles.textTitle}>{item.title}</Text>
        </View>
        <View style={{ alignContent: 'flex-end' }}>
          <Icon name="arrow-right" size={18} color={colors.regular} />
        </View>
      </View>
    </TouchableOpacity>
  );

  render() {
    return (
      <View>
        <View style={styles.header}>
          <Image
            style={styles.headerViewTitle}
            source={require('../../img/logowhite.png')}
            resizeMode="contain"
          />
        </View>
        <ScrollView>
          <FlatList data={menu} renderItem={this.renderItem} />
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(LoginActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Settings);
