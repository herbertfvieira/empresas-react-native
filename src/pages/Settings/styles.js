import { StyleSheet } from 'react-native';
import { colors, metrics } from '../../styles';

const styles = StyleSheet.create({
  header: {
    height: 64,
    backgroundColor: colors.primary,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  headerItem: {
    width: 32,
    paddingLeft: metrics.basePadding / 2,
  },
  headerIcon: {
    height: 32,
  },
  headerViewTitle: {
    width: metrics.screenWidth * 0.7,
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerTextTitle: {
    fontSize: 22,
    color: colors.white,
  },
  searchForm: {
    height: 164,
    marginTop: metrics.baseMargin * 2,
    backgroundColor: colors.primary,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewPicker: {
    backgroundColor: colors.white,
    color: colors.dark,
    borderRadius: metrics.baseRadius,
    height: 48,
    width: metrics.screenWidth * 0.9,
    marginBottom: metrics.baseMargin,
  },
  picker: {
    width: metrics.screenWidth * 0.9,
    color: colors.dark,
  },
  input: {
    backgroundColor: colors.white,
    fontSize: 16,
    color: colors.dark,
    borderRadius: metrics.baseRadius,
    height: 44,
    width: metrics.screenWidth * 0.9,
    paddingLeft: 8,
  },
  button: {
    borderColor: colors.white,
    borderWidth: 1,
    borderRadius: metrics.baseRadius,
    height: 44,
    width: metrics.screenWidth * 0.9,
    marginTop: metrics.baseMargin,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: colors.white,
    fontSize: 16,
  },
  buttonUp: {
    height: 32,
    width: 32,
    paddingBottom: metrics.basePadding,
    margin: metrics.baseMargin,
    justifyContent: 'center',
    alignItems: 'center',
  },

  listItem: {
    flex: 1,
    height: 64,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    borderBottomColor: colors.primary,
    borderBottomWidth: 0.5,
  },

  viewTitle: {
    flex: 1,
    height: 64,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  textTitle: {
    color: colors.regular,
    fontSize: 16,
  },
});

export default styles;
