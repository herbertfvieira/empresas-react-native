import { StyleSheet } from 'react-native';
import { colors, metrics } from '../../styles';

const styles = StyleSheet.create({
  header: {
    height: 64,
    backgroundColor: colors.primary,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  headerItem: {
    width: 32,
  },
  headerViewTitle: {
    width: metrics.screenWidth * 0.7,
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerTextTitle: {
    fontSize: 22,
    color: colors.white,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingBottom: metrics.basePadding,
    marginBottom: metrics.baseMargin,
    borderBottomColor: colors.lighter,
    borderBottomWidth: 1,
  },
  aboutImages: {
    height: 128,
    width: 128,
    borderRadius: 64,
    borderColor: colors.lighter,
    borderWidth: 1,
  },
  aboutViewText: {
    marginBottom: metrics.baseMargin,
  },
  aboutLabel: {
    fontWeight: 'bold',
    fontSize: 16,
    color: colors.regular,
  },
  aboutDesc: {
    fontWeight: 'normal',
    fontSize: 14,
    color: colors.regular,
  },
});

export default styles;
