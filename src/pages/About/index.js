import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView, Text, Image } from 'react-native';

import Icon from 'react-native-vector-icons/SimpleLineIcons';
import styles from './styles';

import { colors, metrics } from '../../styles';

export default class About extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    const { navigation } = this.props;

    return (
      <View>
        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={styles.headerItem}
          >
            <Icon name="arrow-left" size={20} color={colors.white} />
          </TouchableOpacity>
          <View style={styles.headerViewTitle}>
            <Text style={styles.headerTextTitle}>Sobre o App</Text>
          </View>
          <View style={styles.headerItem} />
        </View>

        <ScrollView style={{ padding: metrics.basePadding }}>
          <View style={styles.container}>
            <Image
              style={styles.aboutImages}
              source={require('../../img/iconblack.png')}
              resizeMode="cover"
            />
            <Image
              style={styles.aboutImages}
              source={require('../../img/herbert.png')}
              resizeMode="cover"
            />
          </View>

          <View style={styles.aboutViewText}>
            <Text style={styles.aboutLabel}>Versão</Text>
            <Text style={{ fontWeight: 'regular', color: colors.regular }}>
              1.0
            </Text>
          </View>
          <View style={styles.aboutViewText}>
            <Text style={styles.aboutLabel}>Desenvolvido por</Text>
            <Text style={styles.aboutDesc}>Herbert Elias Filipe Vieira</Text>
          </View>
          <View style={styles.aboutViewText}>
            <Text style={styles.aboutLabel}>Descrição</Text>
            <Text style={styles.aboutDesc}>
              Este aplicativo foi desenvolvido no processo seletivo para a vaga
              de Desenvolvedor React Native na Ioasys. Nele é possível
              visualizar todas as empresas (e algumas de suas informações)
              fornecidas pela api recomendada. Também é possivel pesquisar por
              uma determinada Empresa através de seu nome e ramo.
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}
