import { StyleSheet } from 'react-native';
import { colors, metrics } from '../../styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: metrics.basePadding * 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 240,
    height: 160,
  },
  form: {
    marginTop: metrics.baseMargin * 3,
  },
  input: {
    backgroundColor: colors.white,
    color: colors.dark,
    borderRadius: metrics.baseRadius,
    height: 44,
  },
  buttonLogin: {
    backgroundColor: colors.primary,
    borderRadius: metrics.baseRadius,
    height: 44,
    width: metrics.screenWidth * 0.9,
    marginTop: metrics.baseMargin * 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginText: {
    color: colors.white,
    fontSize: 16,
  },
  buttonPassword: {
    backgroundColor: colors.transparent,
    borderRadius: metrics.baseRadius,
    height: 16,
    width: metrics.screenWidth * 0.9,
    marginTop: metrics.baseMargin * 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonPasswordText: {
    color: colors.regular,
    fontWeight: 'normal',
    fontSize: 14,
  },
  labelText: {
    color: colors.primary,
    fontWeight: 'normal',
    fontSize: 14,
    marginTop: metrics.baseMargin,
  },
});

export default styles;
