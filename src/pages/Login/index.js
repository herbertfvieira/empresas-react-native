import React, { useState, useRef } from 'react';
import '../../config/ReactotronConfig';
import {
  View,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  KeyboardAvoidingView,
} from 'react-native';

import { useDispatch, useSelector } from 'react-redux';
import { signInRequest } from '../../store/modules/auth/actions';

import { colors } from '../../styles';

import styles from './styles';

function Login({ navigation }) {
  const dispatch = useDispatch();
  const passwordRef = useRef();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const loading = useSelector(state => state.auth.loading);

  function login() {
    if (email == null || email == '') {
      Alert.alert('Falha na autenticação', 'O campo Email não pode ser nulo.');
    } else if (password == null || password == '') {
      Alert.alert('Falha na autenticação', 'O campo Senha não pode ser nulo.');
    } else {
      console.tron.log(`Email: ${email} - Senha: ${password}`);
      dispatch(signInRequest(email, password));
    }
  }

  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require('../../img/logo.png')}
        resizeMode="contain"
      />

      <View style={styles.form}>
        <KeyboardAvoidingView behavior="padding" enabled>
          <Text style={styles.labelText}>Email</Text>
          <TextInput
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="Digite seu email"
            underlineColorAndroid={colors.primary}
            keyboardType="email-address"
            selectionColor={colors.primaryLight}
            value={email}
            onChangeText={setEmail}
            returnKeyType="next"
            onSubmitEditing={() => passwordRef.current.focus()}
            blurOnSubmit={false}
          />
          <Text style={styles.labelText}>Senha</Text>
          <TextInput
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="Digite sua senha"
            underlineColorAndroid={colors.primary}
            selectionColor={colors.primaryLight}
            secureTextEntry
            value={password}
            onChangeText={setPassword}
            returnKeyType="send"
            ref={passwordRef}
            onSubmitEditing={login}
            blurOnSubmit
          />
        </KeyboardAvoidingView>

        <TouchableOpacity style={styles.buttonLogin} onPress={login}>
          {loading ? (
            <ActivityIndicator size="small" color={colors.white} />
          ) : (
            <Text style={styles.buttonLoginText}>ENTRAR</Text>
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
}
// }

export default Login;
