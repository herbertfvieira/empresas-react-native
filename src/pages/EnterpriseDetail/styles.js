import { StyleSheet } from 'react-native';
import { colors, metrics } from '../../styles';

const styles = StyleSheet.create({
  header: {
    height: 64,
    backgroundColor: colors.primary,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  headerItem: {
    width: 32,
  },
  headerViewTitle: {
    width: metrics.screenWidth * 0.7,
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerTextTitle: {
    fontSize: 22,
    color: colors.white,
  },
  container: {
    width: metrics.screenWidth,
    height: metrics.screenHeight,
    backgroundColor: colors.white,
    justifyContent: 'flex-end',
    padding: metrics.basePadding,
  },
  loadContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: metrics.screenHeight * 0.8,
  },
  entDetailImages: {
    height: 128,
    width: 128,
    borderRadius: 64,
    borderColor: colors.lighter,
    borderWidth: 1,
  },
  entDetailViewText: {
    marginBottom: metrics.baseMargin,
  },
  entDetailLabel: {
    fontWeight: 'bold',
    fontSize: 16,
    color: colors.regular,
  },
  entDetailDesc: {
    fontWeight: 'normal',
    fontSize: 14,
    color: colors.regular,
  },
});

export default styles;
