import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  View,
  TouchableOpacity,
  ScrollView,
  Text,
  ImageBackground,
  ActivityIndicator,
} from 'react-native';

import Icon from 'react-native-vector-icons/SimpleLineIcons';
import * as EnterprisesActions from '../../store/modules/enterprises/actions';

import { colors } from '../../styles';
import styles from './styles';

class EnterpriseDetail extends Component {
  backToDashboard = () => {
    const { navigation, loadEnterprisesDetailsFailure } = this.props;
    loadEnterprisesDetailsFailure();
    navigation.goBack();
  };

  render() {
    const { enterpriseInfo, loading } = this.props;

    return (
      <View>
        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => this.backToDashboard()}
            style={styles.headerItem}
          >
            <Icon name="arrow-left" size={20} color={colors.white} />
          </TouchableOpacity>

          <View style={styles.headerViewTitle}>
            {loading ? (
              <ActivityIndicator size="small" color={colors.white} />
            ) : (
              <Text style={styles.headerTextTitle}>
                {enterpriseInfo.enterprise_name}
              </Text>
            )}
          </View>
          <View style={styles.headerItem} />
        </View>

        {loading ? (
          <View style={styles.loadContainer}>
            <ActivityIndicator size="large" color={colors.primary} />
          </View>
        ) : (
          <ImageBackground
            source={{
              uri: `http://empresas.ioasys.com.br${enterpriseInfo.photo}`,
            }}
            imageStyle={{ opacity: 0.07 }}
            style={styles.container}
          >
            <ScrollView>
              <View style={styles.entDetailViewText}>
                <Text style={styles.entDetailLabel}>Ramo</Text>
                <Text style={styles.entDetailDesc}>
                  {enterpriseInfo.enterprise_type.enterprise_type_name}
                </Text>
              </View>
              <View style={styles.entDetailViewText}>
                <Text style={styles.entDetailLabel}>Local</Text>
                <Text
                  style={styles.entDetailDesc}
                >{`${enterpriseInfo.city} - ${enterpriseInfo.country}`}</Text>
              </View>
              <View style={styles.entDetailViewText}>
                <Text style={styles.entDetailLabel}>Descrição</Text>
                <Text style={styles.entDetailDesc}>
                  {enterpriseInfo.description}
                </Text>
              </View>
            </ScrollView>
          </ImageBackground>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  enterpriseInfo: state.enterprises.enterpriseSelected,
  loading: state.enterprises.loading,
  auth: state.auth,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(EnterprisesActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EnterpriseDetail);
