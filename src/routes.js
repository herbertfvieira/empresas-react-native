import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import React from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';

import Icon from 'react-native-vector-icons/SimpleLineIcons';
import { colors } from './styles';

import About from './pages/About';
import EnterpriseDetail from './pages/EnterpriseDetail';
import Enterprises from './pages/Enterprises';
import Login from './pages/Login';
import Settings from './pages/Settings';

const Main = createBottomTabNavigator(
  {
    Enterprises: {
      screen: Enterprises,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="home" size={24} color={tintColor} />
        ),
      },
    },
    Settings: {
      screen: Settings,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="settings" size={24} color={tintColor} />
        ),
      },
    },
  },
  {
    defaultNavigationOptions: {
      tabBarOptions: {
        keyboardHidesTabBar: true, // Teclado sobrepõe a barra
        showLabel: false, // Mostra label da aba
        activeTintColor: colors.primary, // cor do icone da aba ativa
        activeBackgroundColor: colors.white, // cor do background da aba ativa
        inactiveTintColor: colors.white, // cor do icone da aba inativa
        style: {
          backgroundColor: colors.primary, // // cor do background da barra de navegação
          height: 60,
        },
        tabStyle: {
          paddingBottom: 6,
        },
      },
    },
  }
);

export default (isSigned = false) =>
  createAppContainer(
    createSwitchNavigator(
      {
        Sign: createSwitchNavigator({
          Login,
        }),
        App: createStackNavigator(
          {
            Main,
            EnterpriseDetail,
            About,
          },
          {
            defaultNavigationOptions: {
              header: null,
            },
          }
        ),
      },

      { initialRouteName: isSigned ? 'App' : 'Sign' }
    )
  );

// export default Routes;
