import { combineReducers } from 'redux';

import auth from './auth/reducer';
import enterprises from './enterprises/reducer';

export default combineReducers({ auth, enterprises });
