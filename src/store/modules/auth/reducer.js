import produce from 'immer';

const INITIAL_STATE = {
  signed: false,
  loading: false,
  client: null,
  uid: null,
  accesstoken: null,
};

export default function auth(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case '@auth/SIGN_IN_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@auth/SIGN_IN_SUCCESS': {
        draft.client = action.payload.client;
        draft.uid = action.payload.uid;
        draft.accesstoken = action.payload.accesstoken;
        draft.signed = true;
        draft.loading = false;
        break;
      }
      case '@auth/SIGN_FAILURE': {
        draft.loading = false;
        break;
      }
      case '@auth/SIGN_OUT': {
        draft.client = null;
        draft.uid = null;
        draft.accesstoken = null;
        draft.signed = false;
        break;
      }
      default:
        return state;
    }
  });
}
