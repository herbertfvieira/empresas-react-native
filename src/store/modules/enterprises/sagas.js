import { Alert } from 'react-native';
import { takeLatest, call, put, all } from 'redux-saga/effects';

import api from '../../../services/api';

import {
  loadEnterprisesSuccess,
  loadEnterprisesFailure,
  loadEnterprisesDetailsSuccess,
  loadEnterprisesDetailsFailure,
} from './actions';

export function* loadEnterprises({ payload }) {
  try {
    const { client, uid, accesstoken } = payload;

    const headers = {
      client,
      uid,
      'access-token': accesstoken,
    };

    const response = yield call(api.get, 'enterprises', {
      params: {},
      headers,
    });

    yield put(loadEnterprisesSuccess(response.data));
  } catch (err) {
    Alert.alert(
      'Falha ao carregar empresas',
      'Houve um erro ao buscar a lista de empresas.'
    );
    yield put(loadEnterprisesFailure());
  }
}

export function* loadFilteredEnterprises({ payload }) {
  try {
    const { client, uid, accesstoken, type, name } = payload;

    const headers = {
      client,
      uid,
      'access-token': accesstoken,
    };

    const url = `enterprises?enterprise_types=${type}&name=${name}`;

    const response = yield call(api.get, url, {
      params: {},
      headers,
    });

    yield put(loadEnterprisesSuccess(response.data));
  } catch (err) {
    Alert.alert(
      'Falha ao carregar empresas',
      'Houve um erro ao buscar a lista de empresas.'
    );
    yield put(loadEnterprisesFailure());
  }
}
export function* loadEnterprisesDetails({ payload }) {
  try {
    const { client, uid, accesstoken, id } = payload;

    const headers = {
      client,
      uid,
      'access-token': accesstoken,
    };

    const url = `enterprises/${id}`;

    const response = yield call(api.get, url, {
      params: {},
      headers,
    });

    yield put(loadEnterprisesDetailsSuccess(response.data));
  } catch (err) {
    Alert.alert(
      'Falha ao carregar detalhes',
      'Verifique sua conexão ou tente mais tarde.'
    );
    yield put(loadEnterprisesDetailsFailure());
  }
}

export default all([
  takeLatest('@enterprises/LOAD_ENTERPRISES_REQUEST', loadEnterprises),
  takeLatest(
    '@enterprises/LOAD_ENTERPRISES_DETAILS_REQUEST',
    loadEnterprisesDetails
  ),
  takeLatest(
    '@enterprises/LOAD_FILTERED_ENTERPRISES_REQUEST',
    loadFilteredEnterprises
  ),
]);
