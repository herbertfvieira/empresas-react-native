import produce from 'immer';

const INITIAL_STATE = {
  enterprisesList: [],
  enterpriseSelected: {
    enterprise_name: '',
    description: '',
    photo: '',
    city: '',
    country: '',
    enterprise_type: {
      enterprise_type_name: '',
    },
  },

  loading: false,
};

export default function user(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case '@enterprises/LOAD_ENTERPRISES_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@enterprises/LOAD_FILTERED_ENTERPRISES_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@enterprises/LOAD_ENTERPRISES_DETAILS_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@enterprises/LOAD_ENTERPRISES_SUCCESS': {
        draft.loading = false;
        draft.enterprisesList = action.payload.data;
        break;
      }
      case '@enterprises/LOAD_ENTERPRISES_FAILURE': {
        draft.loading = false;
        draft.enterprisesList = [];
        break;
      }
      case '@enterprises/LOAD_ENTERPRISES_DETAILS_SUCCESS': {
        draft.loading = false;
        draft.enterpriseSelected = action.payload.data.enterprise;
        break;
      }
      case '@enterprises/LOAD_ENTERPRISES_DETAILS_FAILURE': {
        draft.loading = false;
        draft.enterpriseSelected = {
          enterprise_name: '',
          description: '',
          photo: '',
          city: '',
          country: '',
          enterprise_type: {
            enterprise_type_name: '',
          },
        };
        break;
      }
      default:
        return state;
    }
  });
}
