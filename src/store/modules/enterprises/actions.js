export function loadEnterprisesRequest(client, uid, accesstoken) {
  return {
    type: '@enterprises/LOAD_ENTERPRISES_REQUEST',
    payload: { client, uid, accesstoken },
  };
}

export function loadFilteredEnterprisesRequest(
  client,
  uid,
  accesstoken,
  type,
  name
) {
  return {
    type: '@enterprises/LOAD_FILTERED_ENTERPRISES_REQUEST',
    payload: { client, uid, accesstoken, type, name },
  };
}

export function loadEnterprisesSuccess(data) {
  return {
    type: '@enterprises/LOAD_ENTERPRISES_SUCCESS',
    payload: { data },
  };
}

export function loadEnterprisesFailure() {
  return {
    type: '@enterprises/LOAD_ENTERPRISES_FAILURE',
  };
}

export function loadEnterprisesDetailsRequest(client, uid, accesstoken, id) {
  return {
    type: '@enterprises/LOAD_ENTERPRISES_DETAILS_REQUEST',
    payload: { client, uid, accesstoken, id },
  };
}

export function loadEnterprisesDetailsSuccess(data) {
  return {
    type: '@enterprises/LOAD_ENTERPRISES_DETAILS_SUCCESS',
    payload: { data },
  };
}

export function loadEnterprisesDetailsFailure() {
  return {
    type: '@enterprises/LOAD_ENTERPRISES_DETAILS_FAILURE',
  };
}
