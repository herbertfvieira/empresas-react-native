import { all } from 'redux-saga/effects';

import auth from './auth/sagas';
import enterprises from './enterprises/sagas';

export default function* rootSaga() {
  return yield all([auth, enterprises]);
}
