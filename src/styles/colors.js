export default {
  white: '#FFF',
  lighter: '#E8EAF0',
  light: '#cecece',
  regular: '#969FAA',
  dark: '#666',
  darker: '#333',
  black: '#000',

  primary: '#E51F6B',

  transparent: 'transparent',
};
