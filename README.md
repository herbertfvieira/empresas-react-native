![N|Solid](logo_ioasys.png)

# Desafio React Native - ioasys

Este documento `README.md` tem como objetivo fornecer as informações relacionadas à realização do projeto Empresas.

---

### Objetivo ###

* Desenvolver uma aplicação React Native que consuma a API `Empresas`, cujo Postman esta compartilhado neste repositório (collection).
* Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, **NÃO** é necessário criar um Pull Request para isso.
* Nós iremos realizar a avaliação e te retornar um email com o resultado.

### Screenshots ###

![N|Solid](screens.png)

### Principais dependências utilizadas ###

* `@react-native-community/async-storage` - Persistência do login do usuário.
* `axios` - Realizar requisições à API.
* `redux` - Controle de estados globais da aplicação.
* `redux-persist` - Persistência de dados.
* `redux-saga` - Controle de funções assíncronas.
* `react-navigation` - Estrutura de navegação de todo o App
* `eslint` - Ferramenta de análise estática
* `reactotron` - Ferramenta de depuração

### Passo a passo para a instalação ###

Clone este repositório através da linha de comando 

`git clone https://herbertfvieira@bitbucket.org/herbertfvieira/empresas-react-native.git`

Ou baixe o repositório.

Através do terminal de sua preferência acesse a pasta raiz do projeto e rode:

`yarn` ou `npm install`

Feito isso, inicie um emulador ou conecte um aparelho via usb e rode:

`react-native run-android` ou `react-native run-ios`

Agora é só acessar o aplicativo e visualizar todas suas funcionalidades através das credenciais:

* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234








